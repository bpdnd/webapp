package com.javen.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.javen.dao.ConnectionFactory;
import com.javen.pojo.User;

@Controller
@RequestMapping("/user")
public class UserController extends Basecontroller {

	@ResponseBody
	@RequestMapping(value = "/demo", method = { RequestMethod.POST, RequestMethod.GET })
	public void demo(HttpServletRequest request, HttpServletResponse response) {

		String data = "asdasdasdasdsad";

		List list = new ArrayList();

		list.add(data);
		list.add(data + "34");
		list.add(data + "33333");
		list.add(data + "44444");
		Map<String, ArrayList> map = new HashMap<String, ArrayList>();
		map.put("key", (ArrayList) list);

		this.writeJson(map, response);

	}

	/**
	 * 返回数据库test 表user_t中的数据
	 * 
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "/selectUser", method = { RequestMethod.POST, RequestMethod.GET })
	public void selectUser(HttpServletRequest request, HttpServletResponse response) {

		String heString = request.getParameter("he");
		System.out.println(heString);

		List<User> list = new ArrayList<User>();

		try {
			ConnectionFactory connectionFactory = new ConnectionFactory();
			Connection connection = connectionFactory.makeConnection();

			String selString = "select * from user_t";

			PreparedStatement preparedStatement = connection.prepareStatement(selString);
			ResultSet reuResultSet = preparedStatement.executeQuery();

			while (reuResultSet.next()) {
				User user = new User();
				user.setUser_id(reuResultSet.getInt("user_id"));
				user.setUser_name(reuResultSet.getString("user_name"));
				user.setUser_pass(reuResultSet.getString("user_pass"));
				user.setUser_phone(reuResultSet.getDouble("user_phone"));
				list.add(user);
			}

			preparedStatement.close();
			connection.close();

		} catch (Exception e) {
			// TODO: handle exception
		}

		Map<String, ArrayList> map = new HashMap<String, ArrayList>();
		map.put("key", (ArrayList) list);

		this.writeJson(map, response);

	}

	public static void main(String[] args) {

	}

}
